import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./pages/home";
import NotFound from "./pages/notFound";

const App: React.FC = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>

                <Route exact path="*">
                    <NotFound/>
                </Route>
            </Switch>
        </BrowserRouter>
    );
};

export default App;
