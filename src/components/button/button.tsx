import React from "react";
import "./button.css"

interface MainProps {
    href: string,
    target?: "_self" | "_blank" | "_parent" | "_top"
}

const Button: React.FC<MainProps> = (props) => {
    const target = props.target || "_self";

    return (
        <div className="button-container">
            <a className="button" href={props.href} target={target}>
                {props.children}
            </a>
        </div>
    );
};

export default Button;
