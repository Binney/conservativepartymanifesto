import React from 'react';
import "./container.css"

interface MainProps {
    expand: boolean,
    textAlign: "center" | "left" | "right"
}

const Container: React.FC<MainProps> = (props) => {
    return (
        <div className="container" style={{
            maxWidth: props.expand ? "100%" : "800px",
            textAlign: props.textAlign
        }}>
            {props.children}
        </div>
    )
};

export default Container;
