import React from "react";
import Ticker from "react-ticker";

interface MainProps {
    headlines: string[]
}

const Headlines: React.FC<MainProps> = (props) => {
    function generateHeadlines(): JSX.Element[] {
        let allHeadlines = [
            <span style={{
                margin: "0 20px"
            }}>
                    •
                </span>,
            <span style={{
                whiteSpace: "nowrap"
            }}>
                <b>BREAKING NEWS</b>
            </span>
        ];

        for (const headline of props.headlines) {
            allHeadlines.push(
                <span style={{
                    margin: "0 20px"
                }}>
                    •
                </span>
            );
            allHeadlines.push(
                <span style={{
                    whiteSpace: "nowrap"
                }}>
                    {headline}
                </span>
            );
        }

        return allHeadlines;
    }

    return (
        <div style={{
            backgroundColor: "#FFFF00",
            color: "#333A4D",
            fontFamily: "\"Source Code Pro\", sans-serif"
        }}>
            <Ticker
                mode="smooth">
                {({index}) => (
                    <div style={{}}>
                        {generateHeadlines()}
                    </div>
                )}
            </Ticker>
        </div>
    );
};

export default Headlines;
