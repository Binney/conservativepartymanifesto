import React from 'react';
import "./section.css";

interface MainProps {
    style: React.CSSProperties
}

const Section: React.FC<MainProps> = (props) => {
    return (
        <div className="section" style={props.style}>
            {props.children}
        </div>
    )
};

export default Section;
