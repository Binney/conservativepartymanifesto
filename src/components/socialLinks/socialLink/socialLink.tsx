import React from "react";
import {IconDefinition} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import "./socialLink.css";

interface MainProps {
    href: string,
    icon: IconDefinition
}

const SocialLink: React.FC<MainProps> = (props) => {
    return (
        <a className="social-link" href={props.href} target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={props.icon}/>
        </a>
    );
};

export default SocialLink;
