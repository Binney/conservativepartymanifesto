import React from "react";
import "./socialLinks.css";
import SocialLink from "./socialLink/socialLink";
import {faFacebookF, faPinterestP, faTwitter} from "@fortawesome/free-brands-svg-icons";

interface MainProps {
}

const SocialLinks: React.FC<MainProps> = () => {
    return (
        <div className="social-links">
            <SocialLink
                href="https://www.facebook.com/sharer/sharer.php?u=https://conservativepartymanifesto.uk"
                icon={faFacebookF}/>
            <SocialLink
                href="https://twitter.com/home?status=https://conservativepartymanifesto.uk"
                icon={faTwitter}/>
            <SocialLink
                href="https://pinterest.com/pin/create/button/?url=https://conservativepartymanifesto.uk&media=&description="
                icon={faPinterestP}/>
        </div>
    );
};

export default SocialLinks;
