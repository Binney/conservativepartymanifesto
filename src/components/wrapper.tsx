import React from "react";
import Header from "../sections/header";
import Footer from "../sections/footer";
import SocialLinks from "./socialLinks/socialLinks";

interface MainProps {
}

const Wrapper: React.FC<MainProps> = (props) => {
    return (
        <>
            <Header/>
            <SocialLinks/>
            {props.children}
            <SocialLinks/>
            <Footer/>
        </>
    )
};

export default Wrapper;
