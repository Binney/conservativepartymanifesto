import React, {CSSProperties} from 'react';
import "./body1.css";

interface MainProps {
    style?: CSSProperties
}

const Body1: React.FC<MainProps> = (props) => {
    return (
        <div className="body1-container" style={props.style}>
            <p className="body1">
                {props.children}
            </p>
        </div>
    );
};

export default Body1;
