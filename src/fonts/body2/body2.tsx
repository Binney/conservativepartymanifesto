import React from 'react';
import "./body2.css";

interface MainProps {
}

const Body2: React.FC<MainProps> = (props) => {
    return (
        <div className="body2-container">
            <p className="body2">
                {props.children}
            </p>
        </div>
    );
};

export default Body2;
