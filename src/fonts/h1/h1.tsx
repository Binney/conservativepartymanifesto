import React from 'react';
import "./h1.css";

interface MainProps {
}

const H1: React.FC<MainProps> = (props) => {
    return (
        <div className="h1-container">
            <h1 className="h1">
                {props.children}
            </h1>
        </div>
    );
};

export default H1;
