import React from 'react';
import "./h2.css";

interface MainProps {
}

const H2: React.FC<MainProps> = (props) => {
    return (
        <div className="h2-container">
            <h2 className="h2">
                {props.children}
            </h2>
        </div>
    );
};

export default H2;
