import React from 'react';
import "./quote.css";

interface MainProps {
}

const Quote: React.FC<MainProps> = (props) => {
    return (
        <div className="quote-container">
            <p className="quote">
                <i>{props.children}</i>
            </p>
        </div>
    );
};

export default Quote;
