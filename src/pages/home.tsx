import React from 'react';
import Section from "../components/section/section";
import Container from "../components/container/container";
import Body2 from "../fonts/body2/body2";
import H2 from "../fonts/h2/h2";
import Body1 from "../fonts/body1/body1";
import Quote from "../fonts/quote/quote";
import sunrise from "../images/sunrise.jpg";
import Wrapper from '../components/wrapper';
import Headlines from "../components/headlines";
import Button from "../components/button/button";

interface MainProps {
}

const Home: React.FC<MainProps> = () => {
    return (
        <Wrapper>
            <Headlines headlines={[
                "New economic research proves that poor people would be rich if they just used common sense",
                "Genetic testing reveals Jeremy Corbyn is literally Stalin",
                "Those Germans had it in for us from the start #BlitzSpirit"
            ]}/>
            <Section style={{
                backgroundColor: "#000000",
                color: "#FFFFFF"
            }}>
                <Container expand={false} textAlign="left">
                    <Body2>
                        All you need you know about the Conservative Party's plan for the 2019 election is that they
                        think we're all so small-minded and hateful that we'd be convinced by someone putting
                        "factcheck" in their Twitter username.
                    </Body2>

                    <Body2>
                        Britain faces a simple and inescapable choice — chaos with the Tories, or literally anything
                        else.
                    </Body2>
                </Container>
            </Section>

            <Section style={{
                backgroundColor: "#333A3D",
                color: "#FFFFFF"
            }}>
                <Container expand={false} textAlign="left">
                    <H2>A Brilliant Plan For Brexit</H2>

                    <Body1>
                        Boris has a plan so cunning you slap a pair of glasses on it and call it Michael Gove.
                    </Body1>

                    <Body1>
                        He'll not just stop those filthy Europeans coming here, he'll make Britain such a laughing stock
                        nobody will ever feel the need to visit again.
                    </Body1>

                    <Body1>
                        Then Britain will be a white paradise, just like the good old days, before black people were
                        invented.
                    </Body1>
                </Container>
            </Section>

            <Section style={{
                backgroundColor: "#000000",
                color: "#FFFFFF"
            }}>
                <Container expand={false} textAlign="left">
                    <H2>Trust Us, We Know What's Best For You</H2>

                    <Body1>
                        The Conservative Party vision for Britain is bold, unapologetic, and brave enough to stand up to
                        all those filthy millenials who won't shut up about “social justice” and “basic human decency”.
                    </Body1>

                    <Body1>
                        <ul>
                            <li>
                                <b>Save millions on the NHS</b> by simply letting poor people die!
                            </li>
                            <li>
                                <b>Get Brexit Done</b> so we can focus on what really matters, like tax breaks for the
                                highest earners and covering up Boris's endless affairs.
                            </li>
                            <li>
                                <b>Respect our forebears</b> by continuing the good work Mrs Thatcher started and
                                getting rid of all council housing — after all, it only ever goes to people who insist
                                on voting against the Tories
                            </li>
                        </ul>
                    </Body1>
                </Container>
            </Section>

            <Section style={{
                backgroundImage: `url(${sunrise})`,
                backgroundSize: "cover",
                backgroundPosition: "bottom",
                backgroundAttachment: "fixed",
                color: "#FFFFFF"
            }}>
                <Container expand={false} textAlign="left">
                    <H2>Boris: A Man of The People</H2>

                    <Quote>
                        A MESSAGE FROM OUR DEARLY BELOVED LEADER
                    </Quote>

                    <Body1>
                        We've achieved some great things in the 9 years we've been running the country, like <a
                        href="https://ourworldindata.org/homelessness-rise-england">more than doubling the number of
                        rough sleepers in England</a>, and <a
                        href="https://www.bbc.co.uk/news/uk-england-london-47228698">that lovely bridge</a>.
                    </Body1>

                    <Body1>
                        But three years' worth of attempts to deliver the most important Conservative party policy of
                        our lifetimes have been continually frustrated by Socialist saboteurs.
                    </Body1>

                    <Body1>
                        You, the 0.27% of the population who elected me, know that no matter how bad Brexit gets, no
                        matter if it wrecks the UK economy and tears apart the very fabric of our community and makes us
                        a global laughing stock, it won't be as bad as having <a
                        href="https://yougov.co.uk/topics/politics/articles-reports/2019/06/18/most-conservative-members-would-see-party-destroye">Jeremy
                        Corbyn as prime minister</a>, because he is a Socialist.
                    </Body1>

                    <Body1>
                        After all, in a vibrant, flourishing capitalist economy there will always be some losers, and
                        right now those losers are fairness, honesty, and human rights.
                    </Body1>

                    <Body2>
                        Join me and I will complete what Maggie started. With our combined strength, we can end this
                        destructive conflict and bring order to the galaxy.
                    </Body2>
                </Container>
            </Section>

            <Section style={{
                backgroundColor: "#A31919",
                color: "#FFFFFF"
            }}>
                <Container expand={false} textAlign="left">
                    <H2>Join our mailing list!</H2>

                    <Body1>
                        Oh wait, no, we don't have a mailing list, because unlike Conservative HQ we don't have
                        bucketloads of money to spend on PR, we just bought this domain for £5.99 and put the rest of it
                        together over a few beers.
                    </Body1>

                    <Body1>
                        But you could take a moment to post on Fitter or Twacebook or whatever it is you younguns use to
                        communicate and tell all your mates to
                    </Body1>

                    <Body1>
                        <ul className="pointingList">
                            <li>
                                REGISTER TO VOTE
                            </li>

                            <li>
                                VOTE TACTICALLY, EVEN IF IT MEANS VOTING FOR SOMEONE YOU WOULDN'T USUALLY CONSIDER
                            </li>

                            <li>
                                BUY A HOT DRINK FOR THE NEXT HOMELESS PERSON YOU MEET
                            </li>
                        </ul>
                    </Body1>
                </Container>

                <Container expand={false} textAlign="center">
                    <Button href="https://www.gov.uk/register-to-vote"
                            target="_blank">
                        REGISTER TO VOTE
                    </Button>
                </Container>
            </Section>
        </Wrapper>
    );
};

export default Home;
