import React from 'react';
import Container from "../components/container/container";
import H2 from "../fonts/h2/h2";
import Section from "../components/section/section";
import Body2 from "../fonts/body2/body2";
import Body1 from "../fonts/body1/body1";
import {Link} from "react-router-dom";
import Wrapper from "../components/wrapper";

interface MainProps {
}

const NotFound: React.FC<MainProps> = () => {
    return (
        <Wrapper>
            <Section style={{
                backgroundColor: "#333A3D",
                color: "#FFFFFF"
            }}>
                <Container expand={false} textAlign="left">
                    <H2>Error 404</H2>

                    <Body2>Brexit plan not found.</Body2>

                    <Body1>
                        <Link to="/">Back to 22nd June 2016</Link>
                    </Body1>
                </Container>
            </Section>
        </Wrapper>
    );
};

export default NotFound;
