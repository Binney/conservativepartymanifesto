import React from 'react';
import Body1 from "../fonts/body1/body1";
import Section from "../components/section/section";
import Container from "../components/container/container";

interface MainProps {
}

const Footer: React.FC<MainProps> = () => {
    return (
        <Section style={{
            backgroundColor: "#000000",
            color: "#FFFFFF"
        }}>
            <Container expand={false} textAlign="left">
                <Body1>
                    This is a parody website, by some concerned citizens. It's not affiliated with any political
                    party in any way. You can contribute by <a
                    href="https://gitlab.com/Binney/conservativepartymanifesto">making a merge request</a>! Honk!
                </Body1>
            </Container>
        </Section>
    );
};

export default Footer;
