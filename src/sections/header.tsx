import React from 'react';
import H1 from "../fonts/h1/h1";
import Quote from "../fonts/quote/quote";
import boris from "../images/boris.png";
import Section from "../components/section/section";
import Container from "../components/container/container";

interface MainProps {
}

const Header: React.FC<MainProps> = () => {
    return (
        <Section style={{
            backgroundColor: "#00BAFF",
            color: "#FFFFFF"
        }}>
            <Container expand={false} textAlign="center">

                <div style={{
                    display: "block",
                    margin: "0 auto",
                }}>
                    <img src={boris} alt="Boris Johnson" style={{
                        display: "block",
                        margin: "0 auto",
                        maxWidth: "500px",
                        width: "90%",
                        filter: "grayscale(100%)"
                    }}/>
                </div>

                <H1>The Tories' 2019 Manifesto</H1>

                <Quote>A WEBSITE NOT IN ANY WAY AFFILIATED WITH ANY POLITICAL PARTY</Quote>
            </Container>
        </Section>
    );
};

export default Header;
